package com.admin.panel.controllers;

import com.admin.panel.dto.AuthenticationRequestDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application.properties"
)
class UserControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void login2() {
        AuthenticationRequestDTO authenticationRequestDTO = new AuthenticationRequestDTO();
        authenticationRequestDTO.setEmail("user1@mail.ru");
        authenticationRequestDTO.setPassword("password");
        HttpEntity<AuthenticationRequestDTO> request = new HttpEntity<>(authenticationRequestDTO);
        ResponseEntity<?> responseEntity = restTemplate.exchange("/auth/login", HttpMethod.POST, request, Map.class);
        log.info(responseEntity.getBody().toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}