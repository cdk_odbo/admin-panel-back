package com.admin.panel.controllers;

import com.admin.panel.dto.CategoryDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
        locations = "classpath:application.properties"
)
class CategoryControllerTest {
    private final String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyMUBtYWlsLnJ1Iiwicm9sZSI6IlVTRVIiLCJpYXQiOjE2MTg4MTYyNzgsImV4cCI6MTYxODkxNjI3OH0.LTae7y9hd7fXX4lDtGBaDv_I-dlajJmVzyqPOZj3PAM";

    @Autowired
    private TestRestTemplate restTemplate;


    @Test
    void create() {
        CategoryDto categoryDto = CategoryDto.builder().name("category2")
                .label("label2")
                .icon("icon2")
                .link("link2")
                .status("active")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);
        HttpEntity<CategoryDto> request = new HttpEntity<>(categoryDto, headers);
        ResponseEntity<?> responseEntity = restTemplate.exchange("/categories", HttpMethod.POST, request, CategoryDto.class);
        log.info(responseEntity.toString());
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    void findPaginated() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);

        HttpEntity<?> request = new HttpEntity<>(headers);
        ResponseEntity<?> responseEntity = restTemplate.exchange("/categories", HttpMethod.GET, request, CategoryDto.class);
        log.info(responseEntity.toString());
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

    }

}