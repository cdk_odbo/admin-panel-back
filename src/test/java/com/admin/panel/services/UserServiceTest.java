package com.admin.panel.services;

import com.admin.panel.model.user.Role;
import com.admin.panel.model.user.Status;
import com.admin.panel.model.user.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test.properties")
class UserServiceTest {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;

    @Test
    void save() {
        userService.save(User.builder().email("user14@mail.ru")
                .password(passwordEncoder.encode("password"))
                .role(Role.USER).status(Status.ACTIVE).build());
        assertTrue(userService.findByEmail("user14@mail.ru").isPresent());
    }

}