package com.admin.panel.services;

import com.admin.panel.model.Category;
import com.admin.panel.model.user.Status;
import com.admin.panel.repositories.CategoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.TestPropertySource;

import java.util.List;


@ExtendWith(MockitoExtension.class)
@TestPropertySource(
        locations = "classpath:application.properties"
)
class CategoryTest {
    @Mock
    private CategoryRepository categoryRepository;
    private CategoryService categoryService;
    private AutoCloseable autoCloseable;

    @Test
    void canGetAllCategories() {
        Category category = new Category();
        category.setName("delivery");
        category.setStatus(Status.ACTIVE);
        Category saved = categoryService.save(category);
        List<Category> all = categoryService.findAll();
    }

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        categoryService = new CategoryServiceImpl(categoryRepository);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }
}