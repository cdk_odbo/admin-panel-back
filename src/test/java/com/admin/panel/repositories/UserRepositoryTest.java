package com.admin.panel.repositories;

import com.admin.panel.model.user.Role;
import com.admin.panel.model.user.Status;
import com.admin.panel.model.user.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class UserRepositoryTest {
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void save() {
        User user = User.builder().email("user6@mail.ru").password("password")
                .role(Role.USER).status(Status.ACTIVE).build();
        String encoded = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encoded);
        userRepository.save(user);
        Optional<User> optionalUser = userRepository.findByEmail("user6@mail.ru");
        assertTrue(optionalUser.isPresent());
    }

}