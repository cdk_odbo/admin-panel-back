package com.admin.panel.exceptions;

public class NotDeletedException extends RuntimeException{
    public NotDeletedException() {
    }

    public NotDeletedException(String message) {
        super(message);
    }
}
