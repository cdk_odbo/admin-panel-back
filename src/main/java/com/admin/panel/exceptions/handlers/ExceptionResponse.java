package com.admin.panel.exceptions.handlers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@ToString
public class ExceptionResponse {
    private LocalDateTime timeStamp;
    private String message;
    private String details;

}
