package com.admin.panel.services;


import com.admin.panel.model.Services;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ServicesService {
    Services save(Services services);
    void deleteById(Long id);
    Optional<Services> findById(Long id);
    Page<Services> findByCategoryId(Long id, Pageable pageable);
}
