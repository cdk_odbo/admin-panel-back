package com.admin.panel.services;

import com.admin.panel.dto.UserDto;
import com.admin.panel.model.user.User;

import java.util.Optional;

public interface UserService {
    User save(User user);
    User update(User user);
    Optional<User> findByEmail(String email);
    void deleteById(Long id);
}
