package com.admin.panel.services;

import com.admin.panel.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    Category save(Category category);

    void deleteById(Long id);

    Optional<Category> findById(Long id);

    Page<Category> findPaginatedCategories(Pageable pageable);
    List<Category> findAll();

}
