package com.admin.panel.services;

import com.admin.panel.model.Services;
import com.admin.panel.model.user.Status;
import com.admin.panel.repositories.ServicesRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ServicesServiceImpl implements ServicesService {
    private ServicesRepository servicesRepository;

    public ServicesServiceImpl(ServicesRepository servicesRepository) {
        this.servicesRepository = servicesRepository;
    }

    @Override
    public Services save(Services services) {
        return servicesRepository.save(services);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        servicesRepository.deleteById(id);
    }

    @Override
    public Optional<Services> findById(Long id) {
        return servicesRepository.findById(id);
    }

    @Override
    public Page<Services> findByCategoryId(Long id, Pageable pageable) {
        return servicesRepository.findByCategoryIdAndStatus(id, Status.ACTIVE,pageable);
    }
}
