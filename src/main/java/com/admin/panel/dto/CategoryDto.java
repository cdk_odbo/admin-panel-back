package com.admin.panel.dto;

import com.admin.panel.model.Category;
import com.admin.panel.model.user.Status;
import lombok.*;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CategoryDto {
    private Long id;
    @NotBlank
    private String name;
    @NotBlank
    private String label;
    @NotBlank
    private String icon;
    @NotBlank
    private String link;
    @NotBlank
    private String status;
    private Long parentId;

    public static CategoryDto from(Category category) {
        CategoryDto buildCategory = CategoryDto.builder()
                .id(category.getId())
                .icon(category.getIcon())
                .label(category.getLabel())
                .link(category.getLink())
                .name(category.getName())
                .status(category.getStatus().toString())
                .build();
        if (category.getParent() != null) {
            buildCategory.setParentId(category.getParent().getId());
        }
        return buildCategory;
    }

    public static Category fromDtoToEntity(CategoryDto categoryDto) {
        Category category = new Category();
        category.setStatus(Status.ACTIVE);
        category.setName(categoryDto.getName());
        category.setLabel(categoryDto.getLabel());
        category.setIcon(categoryDto.getIcon());
        category.setLink(categoryDto.getLink());
        return category;
    }
}
