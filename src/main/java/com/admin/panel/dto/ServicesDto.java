package com.admin.panel.dto;

import com.admin.panel.model.Services;
import com.admin.panel.model.user.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ServicesDto {
    private Long id;
    @NotBlank
    private String name;
    @NotBlank
    private String label;
    @NotBlank
    private String icon;
    @NotBlank
    private String link;
    @NotBlank
    private String status;
    @NotNull
    private Long categoryId;


    public static ServicesDto from(Services services) {
        return ServicesDto.builder()
                .id(services.getId())
                .name(services.getName())
                .categoryId(services.getCategory().getId())
                .label(services.getLabel())
                .icon(services.getIcon())
                .link(services.getLink())
                .status(services.getStatus().name())
                .build();

    }

    public static Services fromDtoToEntity(ServicesDto servicesDto) {
        return Services.builder()
                .icon(servicesDto.getIcon())
                .label(servicesDto.getLabel())
                .link(servicesDto.getLink())
                .build();

    }

    public static Status check(String type) {
        if (Status.ACTIVE.name().equalsIgnoreCase("active")) {
            return Status.ACTIVE;
        } else {
            return Status.BANNED;
        }
    }
}
