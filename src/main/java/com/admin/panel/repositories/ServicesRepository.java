package com.admin.panel.repositories;

import com.admin.panel.model.Services;
import com.admin.panel.model.user.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServicesRepository extends JpaRepository<Services, Long> {
    Page<Services> findByCategoryIdAndStatus(Long id, Status status, Pageable pageable);
}
