package com.admin.panel.controllers;

import com.admin.panel.dto.AuthenticationRequestDTO;
import com.admin.panel.exceptions.NotDeletedException;
import com.admin.panel.exceptions.ResourceNotFoundException;
import com.admin.panel.model.user.User;
import com.admin.panel.security.JwtTokenProvider;
import com.admin.panel.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@Slf4j
@RequestMapping("/auth")
public class UserController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;

    public UserController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> authenticate(@RequestBody @Valid AuthenticationRequestDTO request) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
            User user = userService.findByEmail(request.getEmail()).orElseThrow(() -> new UsernameNotFoundException("User doesn't exists"));
            String token = jwtTokenProvider.createToken(request.getEmail(), user.getRole().name());
            Map<Object, Object> response = new HashMap<>();
            response.put("email", request.getEmail());
            response.put("token", token);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>("Invalid email/password combination", HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response, Principal principal) {
        log.info(String.format("User %s tries to log out", principal.getName()));
        SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
        securityContextLogoutHandler.logout(request, response, null);
    }

    @PreAuthorize("hasAuthority('Role_admin')")
    @DeleteMapping("/users/{email}")
    public ResponseEntity<?> deleteById(@PathVariable String email) {
        User user = userService.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(String.format("user with %s not found", email)));
        userService.deleteById(user.getId());
        Optional<User> optional = userService.findByEmail(email);
        if (optional.isPresent()) {
            throw new NotDeletedException(String.format("user with email %s not deleted", email));
        }
        return ResponseEntity.ok("success");
    }
//    @PutMapping("/update")
//    public ResponseEntity<?> update()

}
