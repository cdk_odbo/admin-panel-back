package com.admin.panel.controllers;

import com.admin.panel.dto.ServicesDto;
import com.admin.panel.exceptions.NotDeletedException;
import com.admin.panel.exceptions.ResourceNotFoundException;
import com.admin.panel.model.Services;
import com.admin.panel.services.CategoryService;
import com.admin.panel.services.ServicesService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class ServicesController {
    private final ServicesService servicesService;
    private final CategoryService categoryService;

    public ServicesController(ServicesService servicesService, CategoryService categoryService) {
        this.servicesService = servicesService;
        this.categoryService = categoryService;
    }

    @GetMapping("/services/{id}")
    public ResponseEntity<?> findPaginated(@PathVariable Long id) {
        Services services = servicesService.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(String.format("service with id %d not found", id)));
        ServicesDto dto = ServicesDto.from(services);
        return ResponseEntity.ok(dto);
    }

    @GetMapping("/categories/{id}/services")
    public ResponseEntity<?> findById(@PathVariable Long id, Pageable pageable) {
        Page<ServicesDto> dto = servicesService.findByCategoryId(id, pageable).map(ServicesDto::from);
        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasAuthority('Role_admin')")
    @DeleteMapping("/services/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        servicesService.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(String.format("service with id %d not found", id)));
        servicesService.deleteById(id);
        Optional<Services> optional = servicesService.findById(id);
        if (optional.isPresent()) {
            throw new NotDeletedException(String.format("service with id %d not deleted", id));
        }
        return ResponseEntity.ok("success");
    }

    @PreAuthorize("hasAuthority('Role_admin')")
    @PostMapping("/services")
    public ResponseEntity<?> save(@RequestBody @Valid ServicesDto servicesDto) {
        Services services = ServicesDto.fromDtoToEntity(servicesDto);
        services.setCategory(categoryService.findById(servicesDto.getCategoryId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("category with id %d not found",
                        servicesDto.getCategoryId()))));
        services.setStatus(ServicesDto.check(servicesDto.getStatus()));
        ServicesDto savedServiceDto = ServicesDto.from(servicesService.save(services));
        return ResponseEntity.status(HttpStatus.CREATED).body(savedServiceDto);
    }

    @PutMapping("/services/{id}")
    @PreAuthorize("hasAuthority('Role_admin')")
    public ResponseEntity<?> update(@RequestBody @Valid ServicesDto servicesDto, @PathVariable Long id) {
        if (!servicesService.findById(id).isPresent()) {
            throw new ResourceNotFoundException(String.format("service with id %d not found", id));
        }
        Services services = ServicesDto.fromDtoToEntity(servicesDto);
        services.setCategory(categoryService.findById(servicesDto.getCategoryId())
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("category with id %d not found", servicesDto.getCategoryId()))));
        services.setStatus(ServicesDto.check(servicesDto.getStatus()));
        ServicesDto savedServiceDto = ServicesDto.from(servicesService.save(services));
        return ResponseEntity.status(HttpStatus.CREATED).body(savedServiceDto);
    }


}
