package com.admin.panel.controllers;

import com.admin.panel.dto.CategoryDto;
import com.admin.panel.dto.ServicesDto;
import com.admin.panel.exceptions.NotDeletedException;
import com.admin.panel.exceptions.ResourceNotFoundException;
import com.admin.panel.model.Category;
import com.admin.panel.services.CategoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/categories")
    public ResponseEntity<?> findPaginated(Pageable pageable) {
        Page<Category> paginatedCategories = categoryService.findPaginatedCategories(pageable);
        Page<CategoryDto> paginatedDto = paginatedCategories.map(CategoryDto::from);
        return ResponseEntity.ok(paginatedDto);
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        Category category = categoryService.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Category with id %d not found", id)));
        CategoryDto dto = CategoryDto.from(category);
        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasAuthority('Role_admin')")
    @DeleteMapping("/categories/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        categoryService.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Category with id %d not found", id)));
        categoryService.deleteById(id);
        Optional<Category> optionalCategory = categoryService.findById(id);
        if (optionalCategory.isPresent()) {
            throw new NotDeletedException(String.format("category with id %d not deleted", id));
        }
        return ResponseEntity.ok("success");
    }

    @PreAuthorize("hasAuthority('Role_admin')")
    @PostMapping("/categories")
    public ResponseEntity<?> save(@RequestBody @Valid CategoryDto categoryDto) {
        Category category = null;
        if (categoryDto.getParentId() != null && 0 != categoryDto.getParentId()) {
            category = categoryService.findById(categoryDto.getParentId())
                    .orElseThrow(() -> new ResourceNotFoundException(String.format("category with id %d not found", categoryDto.getId())));
        }
        Category category1 = CategoryDto.fromDtoToEntity(categoryDto);
        if (category != null) {
            category1.setParent(category);
        }
        category1.setStatus(ServicesDto.check(categoryDto.getStatus()));
        Category savedCategory = categoryService.save(category1);
        CategoryDto savedCategoryDto = CategoryDto.from(savedCategory);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedCategoryDto);
    }

    @PreAuthorize("hasAuthority('Role_admin')")
    @PutMapping("/categories/{id}")
    public ResponseEntity<?> update(@RequestBody @Valid CategoryDto categoryDto, @PathVariable Long id) {
        if (!categoryService.findById(id).isPresent()) {
            throw new ResourceNotFoundException(String.format("category with id %d not found", id));
        }
        Category category = categoryService.findById(categoryDto.getParentId())
                .orElseThrow(() -> new ResourceNotFoundException(String.format("category with id %d not found", categoryDto.getId())));
        Category category1 = CategoryDto.fromDtoToEntity(categoryDto);
        category1.setParent(category);
        category1.setStatus(ServicesDto.check(categoryDto.getStatus()));
        category1.setId(id);
        CategoryDto savedCategoryDto = CategoryDto.from(categoryService.save(category));
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(savedCategoryDto);
    }
}
