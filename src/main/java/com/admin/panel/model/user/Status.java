package com.admin.panel.model.user;

public enum Status {
    ACTIVE, BANNED
}
