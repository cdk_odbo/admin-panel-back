package com.admin.panel.model;

import com.admin.panel.model.user.Status;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "categories")
public class Category extends BaseEntity {

    private String name;

    private String label;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String icon;

    private String link;
    @JsonBackReference
    @ManyToOne
    private Category parent;
    @JsonManagedReference
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private Set<Category> subCategories = new HashSet<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "category")
    private Set<Services> services = new HashSet<>();

}
