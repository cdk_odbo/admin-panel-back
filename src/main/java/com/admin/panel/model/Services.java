package com.admin.panel.model;

import com.admin.panel.model.user.Status;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "services")
public class Services extends BaseEntity{

    private String name;

    private String label;

    private String icon;

    private String description;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String link;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

}
